FROM kcmerrill/base

RUN apt-get update && apt-get install -y nodejs npm && apt-get clean
COPY package.json /var/www/html/package.json
RUN npm cache clean -f && npm install -g n && n 5.10.1
RUN npm install -g --save-dev grunt-cli load-grunt-config
RUN ln -s /usr/local/bin/node /usr/bin/node
RUN npm install
